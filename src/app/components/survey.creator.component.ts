import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import * as SurveyCreator from 'survey-creator';
import * as SurveyKo from 'survey-knockout';
import { init as initCustomWidget } from './customwidget';

initCustomWidget(SurveyKo);
SurveyCreator.StylesManager.applyTheme('darkblue');

@Component({
  selector: 'app-survey-creator',
  template: `
    <div id="surveyCreatorContainer"></div>
  `
})
export class SurveyCreatorComponent implements OnInit {
  surveyCreator: SurveyCreator.SurveyCreator;
  @Input() json: any;
  @Output() surveySaved: EventEmitter<object> = new EventEmitter();
  ngOnInit() {
    const options = { showEmbededSurveyTab: true, generateValidJSON: true };
    this.surveyCreator = new SurveyCreator.SurveyCreator(
      'surveyCreatorContainer',
      options
    );

    this.surveyCreator.haveCommercialLicense = true;
    this.surveyCreator.text = JSON.stringify(this.json);
    this.surveyCreator.saveSurveyFunc = this.saveMySurvey;
    this.surveyCreator.showToolbox = 'right';
    this.surveyCreator.showPropertyGrid = 'right';
    this.surveyCreator.rightContainerActiveItem('toolbox');

    this.surveyCreator.JSON = {
        elements: [
            {
                type: 'fullname',
                name: 'question1'
            }
        ]
    };
    // Select the order table component
    this.surveyCreator.selectedElement = this.surveyCreator
        .survey
        .getAllQuestions()[0];


  }

  saveMySurvey = () => {
    console.log(JSON.stringify(this.surveyCreator.text));
    this.surveySaved.emit(JSON.parse(this.surveyCreator.text));
  }
}

/*

 "title": "The Complex Survey",
 "description": "Welcome to the World's most Complex Survey",
 "logoPosition": "top",
 "pages": [
  {
   "name": "page1",
   "elements": [
    {
     "type": "text",
     "name": "question1",
     "title": "What is your name?",
     "valueName": "lastName",
     "isRequired": true,
     "requiredErrorText": "Field required",
     "placeHolder": "Last name"
    },
    {
     "type": "text",
     "name": "question2",
     "visibleIf": "{lastName} notempty",
     "title": "What is your first name?",
     "valueName": "firstName",
     "isRequired": true,
     "requiredErrorText": "Field required",
     "placeHolder": "First name"
    },
    {
     "type": "comment",
     "name": "question3",
     "visibleIf": "{lastName} notempty and {firstName} notempty",
     "valueName": "IDsummary",
     "enableIf": "{lastName} notempty and {firstName} notempty",
     "readOnly": true,
     "titleLocation": "hidden"
    }
   ],
   "title": "Basic Information",
   "description": "To get to know you better"
  }
 ],
 "showProgressBar": "bottom",
 "showTimerPanel": "top"
}*/
