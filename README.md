# SurveyCreatorTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.

## Survey Creator Quick Start

    1. **Create new standard Angular project.**
      ```
      ng new project 
      ```
    2. **Add surveyJS & related dependencies**
      ```
      npm install survey-creator
      npm install survey-angular
      npm install surveyjs-widgets
      npm install bootstrap
      npm install jquery
      ```
    3. **Create survey.creator component && Update app.module declarations array**

    4. **Add survey.creator to app component's template**
      ```
      <app-survey-creator></app-survey-creator>
      ```
    5. **Run**
      ```
      ng serve
      ```
      


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
